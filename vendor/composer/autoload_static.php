<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit1b60cd0afd6b0dd548ac174a73ab7bd6
{
    public static $files = array (
        'cbb79f5c6bbb8e0401616f0e2d693f5c' => __DIR__ . '/../..' . '/app/system/Config.php',
        'ca7f701d76fe08f5bbad394ae43447f3' => __DIR__ . '/../..' . '/app/system/Router.php',
    );

    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit1b60cd0afd6b0dd548ac174a73ab7bd6::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit1b60cd0afd6b0dd548ac174a73ab7bd6::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
