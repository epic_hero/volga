<?php
/**
 * Class ControllerDocs
 **/

namespace App\Controller;

use App\system\Controller;
use App\system\View;
use App\system\DB;
use App\Model\Nav;
use App\Model\Document;

class ControllerDocs extends Controller
{
    /**
     * @var string Название HTML шаблона (resource/Wiew/template)
     * */
    public $template = 'mytemplate';

    /**
     * @var array Передаваемые в шаблон данные
     * */
    public $data = [];

    /**
     * @method ActionIndex
     *
     * @return text/html
     * */
    public function ActionIndex()
    {
        $data = Document::getDocs();
        $this->render('docs\index', $data);
    }

    public function ActionUpdate()
    {
        if($id = $_GET['id']){
            $docs = Document::getDocs($id);
            $this->render('docs\update', $docs);
        } else {
            echo 'Отсутствует обязательный параметр id';
        }
    }
    public function ActionDelete()
    {
        if($id = $_GET['id']){
            $docs = Document::getDocs($id);
            $this->render('docs\update', $docs);
        } else {
            echo 'Отсутствует обязательный параметр id';
        }
    }

    /**
     * @method ActionIndex
     *
     * @return text/html
     * */
    public function ActionCreate()
    {

        if ($get_param = $_GET) {

            $allowed = array('name', 'description', 'short_description', 'position', 'parent_id');
            $values = array($get_param['name'], $get_param['description'], $get_param['short_description'],
                $get_param['position'], $get_param['parent_id']);
            $name = htmlspecialchars($get_param['name']);
            $description = htmlspecialchars($get_param['description']);
            $short_description = htmlspecialchars($get_param['short_description']);
            $db = DB::getIns();
            $db->query("insert into `document` (name) values ({$name})");
        }

        $data = [
            'title' => 'Создание документа',
        ];

        $this->render('docs\create', $data);
    }

    public function render(string $view, array $data)
    {

        if (is_array($data)) {
            extract($data);
        }

        include dirname(dirname(__DIR__)) . '\resource/View/template/header.php';

        include dirname(dirname(__DIR__)) . "\\resource\View\\" . $view . ".php";

        include dirname(dirname(__DIR__)) . '\resource/View/template/footer.php';
    }
}
