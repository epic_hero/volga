<?php

namespace App\Model;

use App\system\DB;
use App\system\View;

class Document
{
    /**
     * @method getAllDocs
     *
     * @return Возвращает список основного меню из БД в тегах <li></li>
     * */
    public static function getDocs($id = null)
    {
        $db = DB::getIns();
        if ($id) {
            return $db->queryAssoc("SELECT * FROM document where id=$id")[0];
        } else {
            return $db->queryAssoc('SELECT * FROM document');
        }
    }

    public static function insertDoc($allowed, $values)
    {
        try {
            $db = DB::getIns();
            $sql = "INSERT INTO document SET " . self::pdoSet($allowed, $values);
            $stm = $db->prepare($sql);
            $stm->execute($values);

        } catch (PDOException $e) {
            die('Ошибка: ' . $e->getMessage());
        }
    }

    public static function delete($id = null)
    {

    }

}
