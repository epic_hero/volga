<?php

return [
    'docs' => [
        'controller' => 'docs',
        'action' => 'index',
    ],
    'docs/create' => [
        'controller' => 'docs',
        'action' => 'create',
    ],
    'docs/update' => [
        'controller' => 'docs',
        'action' => 'update',
    ],
    'docs/delete' => [
        'controller' => 'docs',
        'action' => 'delete',
    ],
];
