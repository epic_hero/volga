<div class="container">
    <div class="card mt-5">
        <div class="card-header">
            <h2>Список документов</h2>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <tr>
                    <th>ID</th>
                    <th>Название</th>
                    <th>Описание</th>
                    <th>Текст</th>
                    <th>Действие</th>
                </tr>
                <?php foreach($data as $doc):?>
                    <tr>
                        <td><?= $doc['id']; ?></td>
                        <td><?= $doc['name']; ?></td>
                        <td><?= $doc['description']; ?></td>
                        <td><?= $doc['value']; ?></td>
                        <td>
                            <a href="/docs/update?id=<?= $doc['id'] ?>" class="btn btn-info">Редактировать</a>
                            <a onclick="return confirm('Are you sure you want to delete this entry?')" href="/docs/delete?id=<?= $doc['id'] ?>" class='btn btn-danger'>Удалить</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>