<form action="/docs/update/" method="get">
    <input type="hidden" class="form-control" id="name" name="id"  value="<?=$data['id']?>">
    <div class="form-group">
        <label for="name">Название</label>
        <input type="text" class="form-control" id="name" name="name" aria-describedby="emailHelp"
               placeholder="" value="<?=$data['name']?>">
    </div>
    <div class="form-group">
        <label for="short_description">Краткое описание</label>
        <input type="text" class="form-control" id="short_description" name="short_description" aria-describedby="emailHelp"
               placeholder="" value="<?=$data['short_description']?>">
    </div>
    <div class="form-group">
        <label for="description">Описание</label>
        <textarea class="form-control" id="description" name="description" rows="3" value="<?=$data['description']?>"></textarea>
    </div>

    <label for="parent_id">Родительский документ</label>
    <select name="parent_id" class="form-control">
        <option value="1"></option>
        <option value="1">Документ 1</option>
        <option value="2">Документ 2</option>
        <option value="3">Документ 3</option>
    </select>
    <div class="form-group">
        <label for="position">Позиция в списке</label>
        <input type="text" class="form-control" id="position" name="position" aria-describedby="emailHelp"
               placeholder="">
    </div>
    <div class="form-group">
        <input type="file" class="form-control-file" name="files" id="files">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
